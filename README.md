# MyNews
![logo](/images/myNews_logo.png)
[![MyNews](/images/google-play-badge.png)](https://play.google.com/store/apps/details?id=silantyevmn.ru.mynews)

Новости России и мира из разных источников.

Все популярные общественные СМИ. Выбирайте рубрики, которые вам интересны, и делитесь новостями с друзьями.
- Только актуальные новости;
- Возможность выбрать наиболее интересную категорию;
- Возможность поиска любой новости;
- Возможность добавить интересные новости в закладки;
- Удобный интерфейс.

Будьте в курсе происходящего и не пропустите всё самое важное и интерсное!

![home_screen](/images/home_screen.jpg)
![bookmarks_screen](/images/bookmarks_screen.jpg)

![category_screen](/images/category_screen.jpg)
![category_pop_screen](/images/category_pop_screen.jpg)

![search_screen](/images/search_screen.jpg)
![web_screen](/images/web_screen.jpg)

# Используемый стэк

- Dagger2
- Moxy
- Cicerone
- RxJava2
- CardView
- RecyclerView
- Retrofit2
- OkHttp3
- Picasso
- PaperDb
- Room
- PrettyTime
- Toasty.

Api взят с ресурса NewsAPI.org

