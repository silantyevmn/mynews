package silantyevmn.ru.mynews.ui.adapter;

public interface IBookmark {
    void onSuccess(boolean isBookmark);
    void onError(String message);
}
