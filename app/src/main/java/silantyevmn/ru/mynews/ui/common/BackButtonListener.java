package silantyevmn.ru.mynews.ui.common;

public interface BackButtonListener {
    boolean onBackPressed();
}
